

class Element:
    def __init__(self, t_melt, t_evap):
        self.t_melt = t_melt
        self.t_evap = t_evap

    def agg_state(self, t, scale='C'):
        pass

    def convert_to_c(self, t, scale):
        pass

class Iron:
    t_melt = 123
    t_evap = 456

class Rectangle:
    # length = 1
    # height = 2

    def __str__(self):
        return f"Rect: L: {self.length}| H: {self.height}"

    def __init__(self, length, height=None):
        self.length = length
        if height is not None:
            self.height = height
        else:
            self.height = self.length

    def am_i_square_question_mark(self):
        return True if self.height == self.length else False


if __name__ == '__main__':
    not_a_square = Rectangle(length=4, height=6)
    print(not_a_square)
    Rectangle.height = 16
    print(not_a_square)
    literally_square = Rectangle(5)
    print(literally_square)
    print(not_a_square.am_i_square_question_mark()) # => l:4 h: 6 -> False
    print(literally_square.am_i_square_question_mark()) # => l == h -> True
    ir = Iron()
    # ir.agg_state(145, 'K')

    iron = Element(123, 456)


class Rectangle:
    length = 1
    height = 1

    def square(self):
        return self.length * self.height


def square(l, h):
    return l * h


rect = Rectangle()
rect.length = 3
rect.height = 8

rect1 = Rectangle()

print(rect.square())
print(rect1.square())


class Student:
    def __init__(self, name):
        self.name = name

    def greeting(self):
        print(f"Hi! I am {self.name}")


st1 = Student('Anna')
st2 = Student('Oleg')

st1.greeting()
st2.greeting()


class Parent:
    name = 'Ivanivanova'

    def greeting(self):
        print(f'Hi! I am {self.name} and I am {self.__class__.__name__}')


class Child(Parent):
    pass

    def greeting(self):
        super().greeting()
        print("Dich'")
        self.send_email()


# par = Parent()
# par.greeting()

child = Child()
print(child.name)
child.greeting()

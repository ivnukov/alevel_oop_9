from lesson1.howtoself import Rectangle


class Car:
    def drive(self):
        print('I can drive!')


class MotorBoat:
    def swim(self):
        print('I can swim')


class Kvadrat(Rectangle):
    pass


class Amphibia(Car, MotorBoat):
    pass


print(__name__)

if __name__ == '__main__':
    amp = Amphibia()
    amp.swim()
    amp.drive()
    sq = Kvadrat(25)

from datetime import datetime


class User:
    def __new__(cls, *args, **kwargs) -> object:
        print("Initiated at {}".format(datetime.now()))
        return super().__new__(cls)

    def __init__(self, name, age) -> None:
        self.name = name
        self.age = age

    def __del__(self) -> None:
        print("Destroyed at {}".format(datetime.now()))

    def __str__(self) -> str:
        return f"User with name {self.name}"

    def __call__(self, nums, *args, **kwargs):
        return sum(nums)

    def __bool__(self):
        return True if self.name == 'Ihor' else False

    def __lt__(self, other):
        return self.age < other.age

    def __eq__(self, other):
        return len(self.name) == len(other.name)

    def __getattr__(self, item):
        print('tryam')
        return self.item

    def get_name(self):
        return self


class A:
    pass


def func():
    pass


if __name__ == '__main__':
    me = User('Ihor', 31)
    not_me = User('Anato', 56)
    print(me, '\n', not_me)
    print(me([1, 5]))
    print(bool(not_me))
    print(me < not_me)
    print(me == not_me)

    print(getattr(me, 'name'))
    print(setattr(me, 'age', 1))
    print(me.age)

    print(delattr(me, 'age'))
    print(me.age)
    print(me)

class User:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def post(self):
        print('I am going to post something.')


class Admin(User):
    def __init__(self, email, name, age):
        self.email = email
        super().__init__(name, age)
        
    def post(self):
        super(Admin, self).post()
        print('I am not banning no one. Just remove al that sh*t')

    def remove(self):
        print('I am going to remove Something')


class SuperAdmin(User):
    def ban_users(self):
        print('I am going to ban someone }:)')

    def post(self):
        print('ehehehehe')
        super().post()
        print('And ban someone')


class SuperDictator(SuperAdmin, Admin):
    pass


if __name__ == '__main__':
    super_adm = SuperDictator('Innokentii', 16)
    print(SuperDictator.__mro__)

    super_adm.post()

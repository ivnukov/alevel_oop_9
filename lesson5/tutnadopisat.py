import time
import random
from csv import DictReader
from datetime import datetime


class Element:
    def __init__(self, t_melt, t_evap):
        self.t_melt = t_melt
        self.t_evap = t_evap

    @staticmethod
    def convert_to_c(temp, scale='C'):
        if scale == 'K':
            return temp + 273
        return temp


class Rectangle:
    def __init__(self, a, b=None):
        self.a = int(a)
        self.b = int(b) if b else a

    def __str__(self):
        return f"{'Rectangle' if self.a != self.b else 'Square'} {self.a}x{self.b}"

    @property
    def square(self):
        return self.a * self.b

    @property
    def parameter(self):
        return 2 * (self.a + self.b)

    @classmethod
    def from_csv(cls, path_to_file):
        figures = []
        try:
            with open(path_to_file) as fp:
                reader = DictReader(fp)
                for row in reader:
                    figures.append(cls(row['a'], row['b'])) # Rectangle(row['a'], row['b']))
        except Exception as err:
            print(f"Unable read file: {err}")
        if figures:
            return figures
        raise ValueError('Unable to create figures.')


def timer(func):
    def wrapper(*args, **kwargs):
        start = datetime.now()
        result = func(*args, **kwargs)
        end = datetime.now()
        print(f"Time taken: {(end - start).seconds} seconds.")
        return result

    return wrapper


def convert(type_=int):
    def converter(func):
        def wrapper(*args, **kwargs):
            print("before")
            result = func(*args, **kwargs)
            print("after")
            result = type_(result)
            return result

        return wrapper

    return converter


@convert(type_=float)
def summa(a, b):
    time.sleep(random.choice([1, 5, 7]))
    return a + b


def main():
    a = summa(*(1, 2))
    nums = dict(a=13, b=26)
    b = summa(**nums)
    print(a, b)
    print(isinstance(a, str))


if __name__ == '__main__':
    # main()
    # rect1 = Rectangle(5, 4)
    # print(rect1.square)
    # print(rect1.parameter)
    # el1 = Element(123, 405)
    # print(el1.convert_to_c(512, 'K'))
    # print(Element.convert_to_c('K'))
    figures_ = Rectangle.from_csv('data.csv')
    [print(f) for f in figures_]
